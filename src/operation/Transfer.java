package operation;
import account.Account;


public class Transfer extends Operation{
	
	private Account to;
	
	
	public void execute(Account account, double amount) {
		account.substract(amount); 
		to.add(amount);
	}


	public Account getTo() {
		return to;
	}


	public void setTo(Account to) {
		this.to = to;
	}





}
