package operation;
import account.Account;


public abstract class Operation {
	
	private String name;
	
	public abstract void execute(Account account, double amount);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
