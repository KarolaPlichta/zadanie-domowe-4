package operation;
import account.Account;


public class Income extends Operation {
	
	
	public void execute(Account account, double amount) {
		
		account.add(amount);
		
	}


}
