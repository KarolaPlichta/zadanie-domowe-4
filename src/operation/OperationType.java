package operation;

public enum OperationType {
	income(true), 
	outcome(false);
	
	boolean operation;
	
	OperationType(boolean type){
		operation = type;
	}

}
