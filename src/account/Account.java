package account;
import java.util.Date;

import operation.Operation;


public class Account {
	
	private String number;
	private History history;
	private double amount;

	
	public Account(String number){
		this.number = number;
		Date date = new Date();
		History history = new History(date);
		history.historyLogShow();
	}

	
	public void add(double amount){
		this.amount+=amount;
	}
	
	public void substract(double amount){
		this.amount-=amount;
	}
	
	public void doOperation(Operation op){
		op.execute(this, amount);
		HistoryLog historylog = new HistoryLog();
		history.addLog(historylog.copy());
		
	}
	

}
