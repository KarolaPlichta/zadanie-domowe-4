package account;
import java.util.Date;

import operation.OperationType;


public class HistoryLog {
	
	private Date dateOfOperation;
	private String title;
	private OperationType operationtype;
	
	
	public Date getDateOfOperation() {
		return dateOfOperation;
	}
	public void setDateOfOperation(Date dateOfOperation) {
		this.dateOfOperation = dateOfOperation;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public OperationType getOperationtype() {
		return operationtype;
	}
	public void setOperationtype(OperationType operationtype) {
		this.operationtype = operationtype;
	}
	
	public HistoryLog copy(){
		HistoryLog historylog = new HistoryLog();
		historylog.setDateOfOperation(getDateOfOperation());
		historylog.setOperationtype(getOperationtype());
		historylog.setTitle(getTitle());
		return historylog;
	}

}
