package account;
import java.util.ArrayList;
import java.util.Date;


public class History {
	
	public ArrayList<HistoryLog> log;
	private Date date;
	
	public History(Date date){
		this.date = date;
		this.log = new ArrayList<HistoryLog>();
		
	}
	
	public void addLog(HistoryLog hist){
		this.log.add(hist);
	}
	
	public void historyLogShow(){
		for(HistoryLog current: log){
			current.getDateOfOperation();
			current.getOperationtype();
			current.getTitle();
		}
	}
	

}
