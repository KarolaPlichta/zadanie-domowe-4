package account;
import operation.Income;
import operation.Operation;
import operation.Transfer;



public class Bank {

	
	public void income(Account account, double amount){
			Operation income = new Income();
			account.doOperation(income);
		
	}
	
	public void transfer(Account from, Account to, double amount){
		Transfer transfer = new Transfer();
		transfer.setTo(to);
		from.doOperation(transfer);
		
	}

}
